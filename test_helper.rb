require 'minitest/autorun'
require 'minitest/reporters'
require 'capybara/webkit'
require 'minitest/capybara'

Minitest::Reporters.use!

Capybara.current_driver = :webkit

Capybara::Webkit.configure do |config|
  # Enable debug mode. Prints a log of everything the driver is doing.
  config.debug = false

  # Allow pages to make requests to any URL without issuing a warning.
  config.allow_unknown_urls

  # Don't load images
  config.skip_image_loading
end

class AcceptanceTest < Minitest::Capybara::Test
  def setup
  end

  def teardown
  end
end
