ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../Gemfile', __FILE__)

require 'bundler/setup' # Set up gems listed in the Gemfile.
require File.expand_path('../test_helper.rb', __FILE__)

Dir.glob('test/**/*_test.rb').each { |f| require File.expand_path("../#{f}", __FILE__) }
