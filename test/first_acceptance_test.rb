class FirstAcceptanceTest < AcceptanceTest
  def test_github_home
    visit 'https://github.com/'

    assert_content 'Welcome home, developers'
    assert_link 'Sign in'
  end
end
